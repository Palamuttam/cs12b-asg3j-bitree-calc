//Author: Rahul Palamuttam, rpalamut@ucsc.edu
// $Id: bitreecalc.java,v 1.5 2013-11-11 22:06:08-08 - - $

//NAME
//   bitreecalc - binary tree RPN calculator

//SYNOPSIS
//   bitreecalc [-e] [-ooutputfile] [inputfile...]

//DESCRIPTION
//   Each line of input is read as a separate statement, and executed
//   as it is read. An input line may have one of four formats:

//   variable = expression
//   The RPN expression is converted into a tree and stored
//   in the symbol table entry associated with the variable.
//   A postorder traversal of the tree is then done in order
//   to evaluate it, using current values of all variables.
//   This new value is then stored in the symbol table
//   associated with the variable. The value and tree are then
//   printed as for the query (?) operator.
//
//   variable ?
//    The tree is traversed and the value of the 
//    variable is recomputed, in case any of the values of
//    the operand variables have changed. The variable is 
//    found in the symbol table and its value and tree are
//    printed. The tree is printed using a fully-parenthesized
//    in-order traversal
//   
//   # anything
//    Any line whose first non-whitespace character is a hash is
//    ignored as a comment.

//OPTIONS
//   Any word of the command line which begins with a minus sign
//   is an option. A word of the command line consisting only of
//   minus sign is an operand, not an option. Options and
//   operands may appear in any order or mixed in any way as 
//   separate flags or single-word flags.
//   -e  Each line of input is echoed to the output verbatim and
//    preceded by four asterisks and a space immediatley before
//    being interpreted. Without this option input is not echoed.
//   -o outputfile
//    Output is written to outputfile. If this option is not set
//    output is written to the standard output. Regardless of -o
//    flag, error messages are always written to the standard error.
//    If the word specifying the -o option is not followed by a
//    filename, then the next word from the command line is used as
//    the output file. If the output file cannot be opened, the 
//    program terminates immediatley with an error message
//   
//   OPERANDS
//    If no input files are specified, the standard input is read.
//    Otherwise each of the filenames specified are read in turn,
//    one after the other as if they were all concatenated together.
//    If an input file is specified as a single-character word which
//    is a minus sign (-) then standard input is read at that point.
//    If an input file can not be opened, an error message is
//    printed and reading continues with the next file.
//   
//   EXIT STATUS
//    0 All input files were valid, no output errors were
//    generated and no invalid options were found.
//   
//    1  An error occurred opening an input or output file.
import java.util.Scanner;
import java.util.NoSuchElementException;
import java.lang.String;
import java.io.*;
class bitreecalc {
   public static final String JAR_NAME = "bitreecalc";
   public static final int EXIT_SUCCESS = 0;
   public static final int EXIT_FAILURE = 1;
   public static final char END_OF_LINE = '\n';
   public static final int ALPHABET_LENGTH = 26;
   private static final String PROG = "bitreecalc: ";
   public static boolean error_status = false;
   public static int exit_status = EXIT_SUCCESS;
   public static symbol_table table = new symbol_table();
   public static linked_stack stack = new linked_stack();
   private static boolean output = false;

   // parser - returns the first non-white space character
   static char parse(String line){
    for(int i = 0; i < line.length() ; i ++){
      char var = line.charAt(i);
      if (!Character.isWhitespace(var)) return line.charAt(i);
    }
    return END_OF_LINE;
   }

   // checks if the character is one of the four operators
   static boolean isoperator(char var){
    return (var == '*' || var == '+' || var == '-' || var == '/');
   }

   //checks if the character is an alphabet
   static boolean isalphabet(char var){
    return !((var < 'a' || var > 'z') && (var < 'A' || var > 'Z'));
   }
   
   // shifts the line over one non-white space character
   static String shift_line (String line){
    return line.substring(line.indexOf(parse(line)) + 1);
   }
   /* prints the value stored in the variable and the tree
    * This function is called after every syntax error free line.
    */
   static void print_after(char varname){
    System.out.println(varname + ": " + table.get_value(varname));
    System.out.printf("   %s%n", table.get_tree(varname));
   }

   /* Assigns an expression tree to a variable. The line is
   * parsed, and each char is put in a tree and pushed onto
   * the stack. Operations pop the tree nodes and link them 
   * as children to the operation tree node
   */
   static void expression(String line, char varname){
    line = shift_line(line);
    error_status = (parse(line) == END_OF_LINE);
    // parses through till the end of the line
    while (parse(line) != END_OF_LINE){
      if (isalphabet(parse(line))){
       stack.push(new bitree(parse(line)));
      } else if (isoperator(parse(line))){
       bitree root = new bitree(parse(line));
       try{
         root.right =(bitree)stack.pop();
         root.left = (bitree)stack.pop();
       } catch (NoSuchElementException error){
         error_status = true;
         return;
       }
       stack.push(root);
      }
      else {
       error_status = true;
      }
      line = shift_line(line);
    }
    bitree result;
    try{
      result = (bitree)stack.pop();
    } catch (NoSuchElementException error){
      error_status = true;
      return;
    }
    // checks for error if stack isn't empty
    if (!stack.empty()){
      error_status = true;
      stack.clear();
      return;
    }
    // puts the resulting tree into the table
    table.put(varname, result.eval(), result);
   }

   // assigns a value to a variable
   static void value(String line, char varname){
    line = shift_line(line);
    try {
      if (Double.isNaN(Double.parseDouble(line))) error_status = true;
      table.put(varname, Double.parseDouble(line), null);
    } catch (NumberFormatException error){
      error_status = true;
    }
   }

   // evaluates the given variable
   static void evaluate(char var){
    bitree tree = table.get_tree(var);
    // checks for a null tree
    Double value = (tree == null) ? table.get_value(var) : tree.eval();
    table.put(var, value, tree);
   }

   // Dumps all the variables that are !(NaN) from the symbol table
   static void table_dump(){
    System.out.println("**** FINAL SYMBOL TABLE ****");
    //for loop that iterates over the table of double values
    for (int index = 0; index < ALPHABET_LENGTH; index++){
      char varname = (char)(index + 'a');
      Double check = table.get_value(varname);
      if(!Double.isNaN(check)) print_after(varname);
    }
   }

   /* The bitreecalc_program is the base implementation of the lab.
    * It gets a scanner passed in and a filename and iterates over
    * all the lines in the scanner. The function uses a switch
    * that will switch between the different operators. Last is a check
    * for error flags and prints the necessary error messages
    */
   static void bitreecalc_program (Scanner scan, String filename){
    char varname;
    //scan's each line
      for (int linecount = 1; scan.hasNextLine(); linecount++){
      String line = scan.nextLine();
      if (output) System.out.println("**** " + line);
      varname = parse(line);
      if (varname == '#') continue;
      line = shift_line(line);
      if (!isalphabet(varname)) error_status = true;
      // operation switch statement
      switch (parse(line)){
      case '=':
       expression(line, varname);
       break;
      case ':':
       value(line, varname);
       break;
      case '?':
       evaluate(varname);
       break;
      default:
       error_status = true;
       break;
      }
      if (error_status){
       error_status = false;
       exit_status = EXIT_FAILURE;
       System.err.println(PROG + filename + ": " + 
                     linecount + ": syntax error");
      } else {
       print_after(varname);
      }
    }   
   }
   
   // The code in main primarily deals with processing the args
   // The basis of the program is in bitreecalc_program()
   public static void main (String[] args) {
    Scanner scan = new Scanner(System.in);
    // flag for stdin
    boolean scan_stdin = true;
    for (int i = 0; i < args.length; i++){
      int length = args[i].length();
      if(args[i].equals("-e")){
       output = true;
      } else if (length > 1 && args[i].substring(0,2).equals("-o")){
       File out = null;
       // checks for the immediate file following -o
       out =  new File(args[i].substring(2));
       try {
         out.createNewFile();
       } catch (IOException NotCreated){
         out = null;
       }
       // another for loop that searches for outputfile
       for (int j = i + 1; j < args.length && out == null; j++){
          out = new File(args[j]);
          i = j;
          try {
             out.createNewFile();
          } catch (IOException NotCreated){
             out = null;
          }
          
       }
       // exits with error if the file was not found
       if (out == null){
          System.err.println(PROG + args[i]+
                        ": No such file or directory");
         System.exit(1);
       }
       // redirects stdout to the file
       try{
         System.setOut(new PrintStream(out));
       } catch (FileNotFoundException excep){
         System.exit(1);
       } 
      } else if (args[i].equals("-")){
       // if - then break out and read from stdin now on
       scan_stdin = true;
       break;
      } else {
       // opens a new scanner with the File specified
       try {
         scan = new Scanner(new FileInputStream(args[i]));
         scan_stdin = false;
       } catch (FileNotFoundException nofile){
         //throws an error and exits if the file was not found
         System.err.println(PROG + args[i]+
                        ": No such file or directory");
         System.exit(EXIT_FAILURE);
       }
       bitreecalc_program(scan, args[i]);
      }
    }
    // checks the flag otherwise skip reading stdin
    if (scan_stdin) bitreecalc_program(scan, "-");
    table_dump();
    System.exit(exit_status);
   }
}




