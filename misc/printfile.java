// $Id: printfile.java,v 1.1 2009-10-27 18:30:36-07 - - $

//
// NAME
//    printfile - illustrate use of printfile and stdout
//
// SYNOPSIS
//    printfile filename words...
//
// DESCRIPTION
//    Writes the command line arguments to a file or to stdout.
//    If args[0] is "-", writes to stdout, else creates a file
//    and writes to it.
//

import java.io.*;
import static java.lang.System.*;

class printfile {
   static String progname = "printfile";

   static void writefile (PrintStream writer, String[] args) {
      out.printf ("writefile:%s%n", args[0]);
      for (String arg: args) writer.printf ("%s%n", arg);
   }

   public static void main (String[] args) {
      if (args.length == 0 || args[0].equals ("-")) {
         writefile (new PrintStream (out), args);
      }else {
         try {
            String filename = args[0];
            PrintStream writer = new PrintStream (new File (filename));
            writefile (writer, args);
            writer.close ();
         }catch (IOException error) {
            err.printf ("%s: %s%n", progname, error.getMessage ());
            exit (1);
         }
      }
   }

}
