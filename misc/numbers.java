// $Id: numbers.java,v 1.2 2012-10-25 19:06:21-07 - - $
import static java.lang.System.*;
class numbers {
   public static void main (String[] args) {
      double inf = 1e300 * 1e300;
      out.println (inf);
      out.println (0.0/0.0);
      out.println (10/0.0);
      out.println (-10/0.0);
      out.println (inf/inf);
      out.println (-inf/inf);
   }
}
