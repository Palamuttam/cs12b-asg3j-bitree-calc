//Author: Rahul Palamuttam, rpalamut@ucsc.edu
// $Id: linked_stack.java,v 1.2 2013-11-11 11:38:16-08 - - $
//
// NAME
//    class linked_stack - implementation of stack
//

import java.util.NoSuchElementException;

class linked_stack<item_t> {

   private class node {
      item_t value;
      node link;
   }

   private node top = null;

   public boolean empty() {
      return top == null;
   }

   // pops an item from the stack
   public item_t pop() {
      if (empty()) {
         throw new NoSuchElementException ("linked_stack.pop");
      }
      item_t returnval = top.value;
      top = top.link;
      return returnval;
   }

   // pushes an item onto the stack
   public void push (item_t value) {
       node next = new node();
       next.value = value;
       next.link = top;
       top = next;
   }
   
   // clears the stack
   public void clear(){
       top = null;
   }

}

