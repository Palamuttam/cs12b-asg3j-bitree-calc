//Author: Rahul Palamuttam, rpalamut@ucsc.edu
// $Id: bitree.java,v 1.3 2013-11-11 11:38:16-08 - - $
//
// NAME
//    class bitree - starter class for bitree implementation.
//

class bitree {
   char symbol;
   bitree left;
   bitree right;

   bitree (char symbol_, bitree left_, bitree right_) {
      symbol = symbol_;
      left = left_;
      right = right_;
   }

   bitree (char symbol_) {
      this (symbol_, null, null);
   }

    // checks if the node is a leaf node
   private boolean isleaf() {
       return (left == null) && (right == null);
   } 

   public String toString () {
      //FIXME
       if(isleaf()) return "" + symbol;
      return "(" + left.toString() + symbol + right.toString() + ")";
   }

    /* Used to evaluate a bitree. It will either return the symbol
     * or recursively call the function on both nodes, with the 
     * corresponding binary operation.
     */
    public double eval(){
        // temporary variable to use instead of bitreecalc.table
        symbol_table table = bitreecalc.table;
        // checks if either left is null or right is null not both
        if(left == null ^ right == null) 
            return (left == null) ? right.eval() : left.eval();
        //checks if operator
        switch(symbol){
        case '*':
            return right.eval() * left.eval();
        case '+':
            return left.eval() + right.eval();
        case '-':
            return left.eval() - right.eval();
        case '/':
            return left.eval() / right.eval();
        }
        // otherwise return the value of the symbol
        return table.get_value(symbol);
    }
}

